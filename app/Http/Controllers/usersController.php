<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class usersController extends Controller
{	
	private $users = [ 
		['name' => 'user1', 'email' => 'user1@domaine.com', 'password' => 'user1pwd'],
    	['name' => 'user2', 'email' => 'user2@domaine.com', 'password' => 'user2pwd'],
    	['name' => 'user3', 'email' => 'user3@domaine.com', 'password' => 'user3pwd']
  	];

    public function showAccueil(){
    	return view('appli.accueil');
    }

    public function showUsers(){
    	$users = $this->users;
    	return view('appli.users',compact('users'));
    }


    public function addUsers(){
        return view('appli.formUser');
    }

    public function postUsers(Request $request){
        $fail = false;
        $errors = array();
        $name = $request->input('nom');
        $email = $request->input('email');
        $pwd = $request->input('pwd');
        if($pwd == ''){
            $fail = true;
            $errors[] = 'Mot de passe obligatoire';
        }
        if($name == ''){
            $fail = true;
            $errors[] = 'Nom obligatoire';
        }
        if($email == ''){
            $fail = true;
            $errors[] = 'Email obligatoire';
        }
        $user = ['name' => $name, 'email' => $email, 'password'=>$pwd];
        
        if($fail){
            //dd($user);
            return view('appli.formUser',['user'=>$user,'errors'=>$errors]); 
        }
        else{
            return view('appli.user',['user'=>$user]);   
        }
       
    }
}
