<html lang="fr">
	<head>
		<title>App Name - @yield('title')</title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="css/styles.css?v=1.0">
	</head>
	<body>
		<ul>
		  <li><a href="accueil">Accueil</a></li>
		  <li><a href="users">Liste des utilisateurs</a></li>
		  <li><a href="add">Ajouter un utilisateur</a></li>
		</ul>
		<div>@include('appli.error', array('errors' => $errors))</div>
		<h1> @yield('titrePage')</h1>
		<div class="container">
			@yield('content')
		</div>		
	</body>
</html>

