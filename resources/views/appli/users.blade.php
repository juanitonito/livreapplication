<html>
	@extends('appli.layout')
	@section('title', 'Titre de la page')
	@section('titrePage','Liste des utilisateurs')
	@section('content')
		<table>
			<tr>
		       <th>Name</th>
		       <th>Email</th>
		       <th>Password</th>
			</tr>
			@foreach ($users as $user)
			<tr>
			   <td>{{ $user['name'] }}</td>
			   <td>{{ $user['email'] }}</td>
			   <td>{{ $user['password'] }}</td>
			</tr>
			@endforeach	
		</table>	
	@endsection
</html>