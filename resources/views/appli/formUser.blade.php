@extends('appli.layout')
@section('title', 'Titre de la page')
@section('titrePage', 'Formulaire utilisateur')
@section('content')
	<form action="add" method="POST">
		<label>Nom </label>
		</br>
		@if (isset($user['name']) )
			<input type="text" name="nom" value="{{ $user['name'] }}"/>

		@else
		   <input type="text" name="nom" value=""/>
		@endif
		</br>

		<label>Email </label>
		</br>
		@if (isset($user['email']))
			<input type="text" name="email" value="{{ $user['email'] }}"/>
		@else
		  <input type="text" name="email" value=""/>
		@endif
		</br>


		<label>Mot de passe </label>
		</br>
		@if (isset($user['password']))
			<input type="password" name="pwd" value="{{ $user['password']}}"/>
		@else
		  <input type="password" name="pwd" value=""/>
		@endif
		
		</br>
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="submit"/>		
	</form>
@endsection